package com.saha.nfcdemo;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Sarbajit Saha on 05-09-2016.
 */
public class SenderActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback
{
    NfcAdapter mNfcAdapter;
    TextView textView;
    EditText msg_edit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);
        textView = (TextView) findViewById(R.id.msg);
        msg_edit = (EditText) findViewById(R.id.msg_edit);
        // Check for available NFC Adapter
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        // Register callback
        mNfcAdapter.setNdefPushMessageCallback(this, this);
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent nfcEvent) {
        String message = msg_edit.getText().toString();
        try{
            Toast.makeText(getApplicationContext(),"This is the message: "+message,Toast.LENGTH_LONG).show();
        } catch (Exception e)
        {
            Log.e("Error sending",e.getMessage());
        }
        NdefRecord ndefRecord = NdefRecord.createMime("text/plain", message.getBytes());
        NdefMessage ndefMessage = new NdefMessage(ndefRecord);
        return ndefMessage;
    }
}

